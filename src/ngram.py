# -*- coding: utf-8 -*-
"""
Created on Sat Dec 23 20:39:13 2017

@author: pedro
"""

import sys
reload(sys)
sys.setdefaultencoding('utf8')

import re

import scipy as sc

emoticons_str = r"""
    (?:
        [:=;] # Eyes
        [oO\-]? # Nose (optional)
        [D\)\]\(\]/\\OpP] # Mouth
    )"""
 
regex_str = [
    emoticons_str,
    r'<[^>]+>', # HTML tags
    r'(?:@[\w_]+)', # @-mentions
    r"(?:\#+[\w_]+[\w\'_\-]*[\w_]+)", # hash-tags
    r'http[s]?://(?:[a-z]|[0-9]|[$-_@.&amp;+]|[!*\(\),]|(?:%[0-9a-f][0-9a-f]))+', # URLs
 
    r'(?:(?:\d+,?)+(?:\.?\d+)?)', # numbers
    r"(?:[a-z][a-z'\-_]+[a-z])", # words with - and '
    r"(?:[0-9][0-9'\-_]+[0-9])",
    r'(?:[\w_]+)', # other words
    
# anything else
]
 
   
tokens_re = re.compile(r'('+'|'.join(regex_str)+')', re.VERBOSE | re.IGNORECASE)
emoticon_re = re.compile(r'^'+emoticons_str+'$', re.VERBOSE | re.IGNORECASE)

from nltk.corpus import stopwords
import string
 
punctuation = list(string.punctuation)
stop =  punctuation + ['rt', 'via', '\\', 'http'] + stopwords.words('portuguese') 
 
def tokenize(s):
    return tokens_re.findall(s)
 
def preprocess(s,lowercase=False, word=' '):
    tokens = s.split(' ')
    if lowercase:
        tokens = [token if emoticon_re.search(token) else token.lower() for token in tokens]
    return tokens
 
tweet = 'RT @marcobonzanini: just an example! :D http://example.com #NLP'
print(preprocess(tweet, 'just'))

from itertools import tee, islice

def ngrams(lst, n):
  tlst = lst
  while True:
    a, b = tee(tlst)
    l = tuple(islice(a, n))
    if len(l) == n:
      yield l
      next(b)
      tlst = b
    else:
      break
 
import csv

from os import listdir
from os.path import isfile, join
import operator
import json
from collections import Counter
import tqdm
from tqdm import *
import pandas
import json
import vincent
import os
import math
import scipy as sc
import numpy as np

mypath = '/home/pedro_barros/Documents/hci/db/twitter_senadores/'

onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
onlyfiles.sort()

def tweet(e):
    s = []
    with open(mypath+e, 'r') as f:
        reader = csv.DictReader(f)
        for row in reader:
            terms_only = [term for term in preprocess(row['text'].decode("utf-8"), True)
                  if term not in stop and
                  not term.startswith(('\\', 'https', 'http'))
                  and len(term) > 2 ]
            punctuation = list(string.punctuation)
            for p in punctuation:
                terms_only = [x.replace(p, '') for x in terms_only]
            s.append(terms_only)
    return s

def count(n, e):
       
    count_all = Counter()
    #for e in tqdm(onlyfiles):
    #for e in onlyfiles:
    with open(mypath+e, 'r') as f:
            reader = csv.DictReader(f)
            for row in reader:
                terms_only = [term for term in preprocess(row['text'].decode("utf-8"), True)
                      if term not in stop and
                      not term.startswith(('\\', 'https', 'http'))
                      and len(term) > 2 ]
                punctuation = list(string.punctuation)
                for p in punctuation:
                    terms_only = [x.replace(p, '') for x in terms_only]
               
                count_all.update(ngrams(terms_only, n))
    return count_all
    
w_good = [ 'positivo', 'afortunado', 'correto', 'superior', 'rapido', u'legítmo', u'constitucional', u'avanço', u'atacar']
w_bad = ['negativo', 'desafortunado', 'incorreto', 'inferior', 'lento', u'inlegítmo' , u'inconstitucional ', u'retrocesso', u'defender']

def count_words(t):
    total_pos = 0 
    total_neg = 0
    total = 0
    
    for idx in range(len(w_good)):
        ref_pos = w_good[idx]
        ref_neg = w_bad[idx]
    
        for tweet in t:
            if ref_pos in tweet:
                total_pos = 1 + total_pos
                
            if ref_neg in tweet:
                total_neg = 1 + total_neg
                
            total = total + 1
        
    return total, total_pos, total_neg


def TMI_1(word, ref_pos, ref_neg, tweet, s, total, total_pos, total_neg):
    
    count_word_pos = 0
    count_word_neg = 0
    count_word = 0
        
    try:
        count_word_pos = s[(word,ref_pos)]
    except:
        count_word_pos = 0
        
    try:
        count_word_pos = s[(ref_pos, word)]
    except:
        count_word_pos = count_word_pos
        
   # try:
    #    count_word_neg = s[(word,ref_neg)]
    #except:
     #   count_word_neg = 0
    
    if count_word_pos >= 3 :#and count_word_neg >= 3:
        if total_pos == 0:
            prob = 0
        else :
            prob = (float(count_word_pos)/total_pos) #/ (float(count_word_neg)/total_neg)   
            #prob = np.log2(prob)
    
    else:
        
        prob = 0
    
    return count_word_pos, count_word_neg

    
def main():
    for e in onlyfiles:
        print e
        c = count(2, e)
        t = tweet(e)
        s = []
        
        total, total_pos, total_neg = count_words(t)
        print total, total_pos, total_neg
        for p in tqdm(t):
            for text in p:
                    tmi_p = 0
                    tmi_n = 0
                    print '-----------------', text
                    for idx in range(len(w_good)):
                        tmi_p, tmi_n = TMI_1(text, w_good[idx], w_bad[idx], t, c, total, total_pos, total_neg)
                        print 'tmi_p = ', tmi_p
                        if tmi_p != 0 :
                            print 'Pos - word :', text, '-',w_good[idx], 'prob : ', tmi_p
                        if tmi_n != 0 :
                            print 'Neg - word :', text, '-',w_neg[idx], 'prob : ', tmi_n
                        

        print '-----------------------------------'

if __name__ == "__main__":
    main()
                
