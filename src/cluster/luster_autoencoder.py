# -*- coding: utf-8 -*-
"""
Created on Wed Jan 17 09:26:53 2018

@author: pedro_barros
"""

import sys
reload(sys)
sys.setdefaultencoding('utf8')

import re

import scipy as sc

emoticons_str = r"""
    (?:
        [:=;] # Eyes
        [oO\-]? # Nose (optional)
        [D\)\]\(\]/\\OpP] # Mouth
    )"""
 
regex_str = [
    emoticons_str,
    r'<[^>]+>', # HTML tags
    r'(?:@[\w_]+)', # @-mentions
    r"(?:\#+[\w_]+[\w\'_\-]*[\w_]+)", # hash-tags
    r'http[s]?://(?:[a-z]|[0-9]|[$-_@.&amp;+]|[!*\(\),]|(?:%[0-9a-f][0-9a-f]))+', # URLs
 
    r'(?:(?:\d+,?)+(?:\.?\d+)?)', # numbers
    r"(?:[a-z][a-z'\-_]+[a-z])", # words with - and '
    r"(?:[0-9][0-9'\-_]+[0-9])",
    r'(?:[\w_]+)', # other words
    
# anything else
]
 
   
tokens_re = re.compile(r'('+'|'.join(regex_str)+')', re.VERBOSE | re.IGNORECASE)
emoticon_re = re.compile(r'^'+emoticons_str+'$', re.VERBOSE | re.IGNORECASE)

from nltk.corpus import stopwords
import string
 
punctuation = list(string.punctuation)
stop =  punctuation + ['rt', 'via', '\\', 'http'] + stopwords.words('portuguese') 
 
def tokenize(s):
    return tokens_re.findall(s)
 
def preprocess(s,lowercase=False, word=' '):
    tokens = s.split(' ')
    if lowercase:
        tokens = [token if emoticon_re.search(token) else token.lower() for token in tokens]
    return tokens
 
tweet = 'RT @marcobonzanini: just an example! :D http://example.com #NLP'
print(preprocess(tweet, 'just'))

from itertools import tee, islice

def ngrams(lst, n):
  tlst = lst
  while True:
    a, b = tee(tlst)
    l = tuple(islice(a, n))
    if len(l) == n:
      yield l
      next(b)
      tlst = b
    else:
      break
 
import csv

from os import listdir
from os.path import isfile, join
import operator
import json
from collections import Counter
import tqdm
from tqdm import *
import pandas
import json
import vincent
import os
import math
import scipy as sc
import numpy as np

import gensim
import logging
import numpy as np

from keras_dec import DeepEmbeddingClustering

mypath = '../../db/twitter_senadores/'

onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
onlyfiles.sort()

def tweet(e):
    s = []
    id_only = []
    with open(mypath+e, 'r') as f:
        reader = csv.DictReader(f)
        for row in reader:
            terms_only = [term for term in preprocess(row['text'].decode("utf-8"), True)
                  if term not in stop and
                  not term.startswith(('\\', 'https', 'http', '#'))
                  and len(term) > 2 ]
            id_only.append(int(row['id']))
            punctuation = list(string.punctuation)
            for p in punctuation:
                terms_only = [x.replace(p, '').lower() for x in terms_only]
            s.append(terms_only)
    return s, id_only

def count(n, e):
       
    count_all = Counter()
    #for e in tqdm(onlyfiles):
    #for e in onlyfiles:
    with open(mypath+e, 'r') as f:
            reader = csv.DictReader(f)
            for row in reader:
                terms_only = [term for term in preprocess(row['text'].decode("utf-8"), True)
                      if term not in stop and
                      not term.startswith(('\\', 'https', 'http'))
                      and len(term) > 2 ]
                punctuation = list(string.punctuation)
                for p in punctuation:
                    terms_only = [x.replace(p, '') for x in terms_only]
               
                count_all.update(ngrams(terms_only, n))
    return count_all
    

rep = '../../rep/'

s = []
l = []
'''
with open(rep+'fdist_sen.csv', 'r') as f:
    reader = csv.DictReader(f)
    for row in reader:
        i = int(row['freq'])
        bigram = row['bigram'].replace(', ', ',')[1:-1]
        bigram = bigram.split(',')
        bigram = (bigram[0][2:-1],bigram[1][2:-1])
        if i > 20:
            print bigram, ' - ', i
        else:
            break
'''
bigram = (u'cpi', u'animais')

def get_list(bigram):
    l = []
    for e in tqdm(onlyfiles):
        c = count(2, e)
        if bigram in c:
           if c[bigram] > 5:
               l.append(e)
    return l

'''

l = get_list(bigram) 
set_clust[bigram] = l

with open(rep + 'list_cluster.csv', 'wb') as f:
    writer = csv.writer(f)
    writer.writerow(["bigram","quant", "senador"])
    for e in set_clust:
        aux = [[str(e).encode("utf-8"),len(set_clust[e]), set_clust[e]]]
        writer.writerows(aux)

'''


logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s',
                        level=logging.INFO)

# load vanilla model
vanilla_model = "./lx-dsemvectors/models/vanilla/wikipedia.vanilla.w2v"
model = gensim.models.Word2Vec.load(vanilla_model)

model.init_sims(replace=True)

vocab = set(model.index2word)

set_sen = {}

def t2v(tweet, dimention = 300):
    l = np.zeros(0)
    idx = 0
    for e in tweet:
        if e in vocab:
            idx = idx + 1
            l = np.concatenate((l,model[e]), axis = 0)
    l = np.concatenate((l, np.zeros(((dimention-idx)* 100))))
    l = l.reshape(dimention * 100)
    return l

map_sen = {}
map_sen_id = {}

x_train = []

onlyfiles = onlyfiles

for e in tqdm(onlyfiles):
    aux, id_l = tweet(e)
    l = []
    for idx, t in enumerate(aux):
        if 'presidente' in t and u'dilma' in t:
            l.append(t2v(t, dimention = 29))
        
    map_sen[e] = l
    map_sen_id[e] = id_l

id_l = []

for e in onlyfiles:
    for idx, t in enumerate(map_sen[e]):
         map_sen[e][idx] = (t+0.49)*5
         x_train.append(map_sen[e][idx])
         id_l.append(map_sen_id[e][idx])
         map_sen[e][idx] = 0
         

#print id_l[0]

x_train = np.array(x_train)
X = x_train.astype('float32')

label = {}

with open(mypath+'../../rep/Clustering_words_text.csv', 'r') as f:
            reader = csv.DictReader(f)
            for row in reader:
                label[int(row['id'])] = int(row['class'])
                
for idx, e in enumerate(id_l):
    id_l[idx] = label[e]
    

y = np.array(id_l)
y = y.astype(int)


import matplotlib.pyplot as plt

from sklearn.decomposition import PCA
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis

from random import shuffle

from matplotlib import colors as mcolors


target_names = []
for e in range(13):
    target_names.append('class '+str(e))
target_names = np.array(target_names)

pca = PCA(n_components=2)
#X_r = pca.fit(X).transform(X)

lda = LinearDiscriminantAnalysis(n_components=2)
X_r = lda.fit(X, y).transform(X)

#print('explained variance ratio (first two components): %s'
#      % str(pca.explained_variance_ratio_))

plt.figure()

colors = []
s = mcolors.cnames

for e in s:
    colors.append(e)

colors = colors[:13]


lw = 2

for color, i, target_name in zip(colors, range(13), target_names):
    plt.scatter(X_r[y == i, 0], X_r[y == i, 1], color=color, alpha=.8, lw=lw,
                label=target_name)
plt.legend(loc='best', shadow=False, scatterpoints=1)
plt.title('PCA of IRIS dataset')

'''
c = DeepEmbeddingClustering(n_clusters=13, input_dim=29 * 100)
c.initialize(x_train, finetune_iters=100000, layerwise_pretrain_iters=50000)
y_pred = c.cluster(x_train)

print y_pred

aux = []

print 'id, label'
#for idx, e in enumerate(id_l):
	#print id_l[idx], ',', y_pred[idx]

with open('Clustering_words_1.csv', 'wb') as f:    
         writer = csv.writer(f)
         writer.writerow(["id", "label"])
         for idx, e in enumerate(id_l):
             aux.append([id_l[idx], y_pred[idx]])
         writer.writerows(aux)

'''