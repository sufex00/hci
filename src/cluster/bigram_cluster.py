# -*- coding: utf-8 -*-
"""
Created on Tue Jan 16 11:42:56 2018

@author: pedro_barros
"""

import sys
reload(sys)
sys.setdefaultencoding('utf8')

import re

import scipy as sc

emoticons_str = r"""
    (?:
        [:=;] # Eyes
        [oO\-]? # Nose (optional)
        [D\)\]\(\]/\\OpP] # Mouth
    )"""
 
regex_str = [
    emoticons_str,
    r'<[^>]+>', # HTML tags
    r'(?:@[\w_]+)', # @-mentions
    r"(?:\#+[\w_]+[\w\'_\-]*[\w_]+)", # hash-tags
    r'http[s]?://(?:[a-z]|[0-9]|[$-_@.&amp;+]|[!*\(\),]|(?:%[0-9a-f][0-9a-f]))+', # URLs
 
    r'(?:(?:\d+,?)+(?:\.?\d+)?)', # numbers
    r"(?:[a-z][a-z'\-_]+[a-z])", # words with - and '
    r"(?:[0-9][0-9'\-_]+[0-9])",
    r'(?:[\w_]+)', # other words
    
# anything else
]
 
   
tokens_re = re.compile(r'('+'|'.join(regex_str)+')', re.VERBOSE | re.IGNORECASE)
emoticon_re = re.compile(r'^'+emoticons_str+'$', re.VERBOSE | re.IGNORECASE)

from nltk.corpus import stopwords
import string
 
punctuation = list(string.punctuation)
stop =  punctuation + ['rt', 'via', '\\', 'http'] + stopwords.words('portuguese') 
 
def tokenize(s):
    return tokens_re.findall(s)
 
def preprocess(s,lowercase=False, word=' '):
    tokens = s.split(' ')
    if lowercase:
        tokens = [token if emoticon_re.search(token) else token.lower() for token in tokens]
    return tokens
 
tweet = 'RT @marcobonzanini: just an example! :D http://example.com #NLP'
print(preprocess(tweet, 'just'))

from itertools import tee, islice

def ngrams(lst, n):
  tlst = lst
  while True:
    a, b = tee(tlst)
    l = tuple(islice(a, n))
    if len(l) == n:
      yield l
      next(b)
      tlst = b
    else:
      break
 
import csv

from os import listdir
from os.path import isfile, join
import operator
import json
from collections import Counter
import tqdm
from tqdm import *
import pandas
import json
import vincent
import os
import math
import scipy as sc
import numpy as np

import gensim
import logging
import numpy as np


def ngram_cluster(n, num):
    count_all = Counter()
    with open('/home/pedro_barros/Documents/hci/rep/cluster_result_k=30.csv', 'r') as f:
                reader = csv.DictReader(f)
                for row in reader:
                    if int(row['class']) == num:
                        terms_only = [term for term in preprocess(row['text'].decode("utf-8"), True)
                              if term not in stop and
                              not term.startswith(('\\', 'https', 'http'))
                              and len(term) > 2 ]
                        punctuation = list(string.punctuation)
                        for p in punctuation:
                            terms_only = [x.replace(p, '') for x in terms_only]
                       
                        count_all.update(ngrams(terms_only, n))
    return count_all
    
for num in tqdm(range(30,31)):
    with open('/home/pedro_barros/Documents/hci/rep/ngram_cluster/label_'+str(num)+'.csv', 'wb') as f:
            writer = csv.writer(f)
            writer.writerow(["ngram_1","ngram_2","freq"])
            c = ngram_cluster(n, num).most_common(50)
            for  idx, e in enumerate(c):
                aux = [[c[idx][0][0].encode('utf-8'), c[idx][0][1].encode('utf-8'), c[idx][1]]]
                writer.writerows(aux)

map_id = {}    

for e in tqdm(onlyfiles):
    with open(mypath+e, 'r') as f:
                reader = csv.DictReader(f)
                for row in reader:
                    map_id[int(row['id'])] = e

for num in range(0,30):
    set_sen = set()
    with open('/home/pedro_barros/Documents/hci/rep/cluster_result_k=30.csv', 'r') as f:
        reader = csv.DictReader(f)
        for row in reader:
            if int(row['class']) == num:
                set_sen.add(map_id[int(row['id'])])
    print 'class = ', num, 'len = ',len(set_sen)