# -*- coding: utf-8 -*-
"""
Created on Sat Jan 13 10:42:32 2018

@author: pedro
"""


import sys
reload(sys)
sys.setdefaultencoding('utf8')

import re

import scipy as sc

emoticons_str = r"""
    (?:
        [:=;] # Eyes
        [oO\-]? # Nose (optional)
        [D\)\]\(\]/\\OpP] # Mouth
    )"""
 
regex_str = [
    emoticons_str,
    r'<[^>]+>', # HTML tags
    r'(?:@[\w_]+)', # @-mentions
    r"(?:\#+[\w_]+[\w\'_\-]*[\w_]+)", # hash-tags
    r'http[s]?://(?:[a-z]|[0-9]|[$-_@.&amp;+]|[!*\(\),]|(?:%[0-9a-f][0-9a-f]))+', # URLs
 
    r'(?:(?:\d+,?)+(?:\.?\d+)?)', # numbers
    r"(?:[a-z][a-z'\-_]+[a-z])", # words with - and '
    r"(?:[0-9][0-9'\-_]+[0-9])",
    r'(?:[\w_]+)', # other words
    
# anything else
]
 
   
tokens_re = re.compile(r'('+'|'.join(regex_str)+')', re.VERBOSE | re.IGNORECASE)
emoticon_re = re.compile(r'^'+emoticons_str+'$', re.VERBOSE | re.IGNORECASE)

from nltk.corpus import stopwords
import string
 
punctuation = list(string.punctuation)
stop =  punctuation + ['rt', 'via', '\\', 'http'] + stopwords.words('portuguese') 
 
def tokenize(s):
    return tokens_re.findall(s)
 
def preprocess(s,lowercase=False, word=' '):
    tokens = s.split(' ')
    if lowercase:
        tokens = [token if emoticon_re.search(token) else token.lower() for token in tokens]
    return tokens
 
tweet = 'RT @marcobonzanini: just an example! :D http://example.com #NLP'
print(preprocess(tweet, 'just'))

from itertools import tee, islice

def ngrams(lst, n):
  tlst = lst
  while True:
    a, b = tee(tlst)
    l = tuple(islice(a, n))
    if len(l) == n:
      yield l
      next(b)
      tlst = b
    else:
      break
 
import csv

from os import listdir
from os.path import isfile, join
import operator
import json
from collections import Counter
import tqdm
from tqdm import *
import pandas
import json
import vincent
import os
import math
import scipy as sc
import numpy as np

import gensim
import logging
import numpy as np

mypath = '/home/pedro_barros/Documents/hci/db/twitter_senadores/'

onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
onlyfiles.sort()

def tweet(e):
    s = []
    id_only = []
    with open(mypath+e, 'r') as f:
        reader = csv.DictReader(f)
        for row in reader:
            terms_only = [term for term in preprocess(row['text'].decode("utf-8"), True)
                  if term not in stop and
                  not term.startswith(('\\', 'https', 'http', '#'))
                  and len(term) > 2 ]
            id_only.append(row['id'])
            punctuation = list(string.punctuation)
            for p in punctuation:
                terms_only = [x.replace(p, '').lower() for x in terms_only]
            s.append(terms_only)
    return s, id_only

def count(n, e):
       
    count_all = Counter()
    #for e in tqdm(onlyfiles):
    #for e in onlyfiles:
    with open(mypath+e, 'r') as f:
            reader = csv.DictReader(f)
            for row in reader:
                terms_only = [term for term in preprocess(row['text'].decode("utf-8"), True)
                      if term not in stop and
                      not term.startswith(('\\', 'https', 'http'))
                      and len(term) > 2 ]
                punctuation = list(string.punctuation)
                for p in punctuation:
                    terms_only = [x.replace(p, '') for x in terms_only]
               
                count_all.update(ngrams(terms_only, n))
    return count_all
    

cnn = '/home/pedro_barros/Documents/cnn_sen/'

sen_map = []
sen_map_id = []
sen_map_text = []
map_id={}
map_id_text = {}
for idx, e in tqdm(enumerate(onlyfiles)): 
    l = []
    l_id = []
    if idx < 80:
        with open(cnn+'cnn_'+e, 'r') as f:
            reader = csv.DictReader(f)
            for row in reader:
                aux = row['cnn_vec']
                aux = aux[1:-1]
                l = []
                for num in aux.split(','):
                    l.append(float(num))
                
                aux = int(row['id'])
                #sen_map_id.append(map_id_text[aux])
                sen_map.append(l[:60])
                sen_map_id.append(aux)

x = []
y = []

with open('/home/pedro_barros/Documents/hci/rep/Clustering_words_text.csv', 'r') as f:
    reader = csv.DictReader(f)
    for row in reader:
        map_id [int(row['id'])] = int(row['class'])
        map_id_text [int(row['id'])] = row['text']
        if 'presidente' in row['text'] and 'dilma' in row['text']:
            y.append(int(row['class']))



for idx, e in tqdm(enumerate(onlyfiles)): 
    l = []
    l_id = []
    if idx < 80:
        with open(cnn+'cnn_'+e, 'r') as f:
            reader = csv.DictReader(f)
            for row in reader:
                aux_id = int(row['id'])
                #sen_map_id.append(map_id_text[aux])
                if 'presidente' in map_id_text[aux_id] and 'dilma' in map_id_text[aux_id]:
                     aux = row['cnn_vec']
                     aux = aux[1:-1]
                     l = []
                     for num in aux.split(','):
                         l.append(float(num))
                     x.append(l[:60])
                sen_map_text.append(map_id_text[aux_id])

import matplotlib.pyplot as plt

from sklearn import datasets
from sklearn.decomposition import PCA
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from matplotlib import colors as mcolors
from random import shuffle

iris = datasets.load_iris()

X = np.array(x)
X = X.astype('float32')
y = np.array(y)
y = y.astype('float32')

target_names = ['Class '+str(e) for e in range(13)]

pca = PCA(n_components=2)
X_r = pca.fit(X).transform(X)

lda = LinearDiscriminantAnalysis(n_components=2)
X_r2 = lda.fit(X, y).transform(X)

# Percentage of variance explained for each components
print('explained variance ratio (first two components): %s'
      % str(pca.explained_variance_ratio_))

plt.figure()

s = mcolors.cnames

colors = []
for e in s:
    colors.append(e)

shuffle(colors)

colors = colors[:13]

lw = 2
'''
for color, i, target_name in zip(colors, range(13), target_names):
    plt.scatter(X_r[y == i, 0], X_r[y == i, 1], color=color, alpha=.8, lw=lw,
                label=target_name)
plt.legend(loc='best', shadow=False, scatterpoints=1)
plt.title('PCA of IRIS dataset')
'''
plt.figure()
for color, i, target_name in zip(colors, range(13), target_names):
    plt.scatter(X_r2[y == i, 0], X_r2[y == i, 1], alpha=.2, color=color,
                label=target_name)
plt.legend(loc='best', shadow=False, scatterpoints=1)
plt.title('LDA of IRIS dataset')

plt.show()

        
        
'''
from sklearn.cluster import KMeans

list_la = []

for num in range(8, 15):
    
    k = num
    
    # Number of clusters
    kmeans = KMeans(n_clusters=k, n_jobs=-1)
    # Fitting the input data
    kmeans = kmeans.fit(sen_map)
    # Getting the cluster labels
    labels = kmeans.predict(sen_map)
    
    list_la.append(kmeans.inertia_)    
    
    # Centroid values
    centroids = kmeans.cluster_centers_
    
    print 'k=', num, '- cost:', kmeans.inertia_
    
map_id = {}    

for e in tqdm(onlyfiles):
    with open(mypath+e, 'r') as f:
                reader = csv.DictReader(f)
                for row in reader:
                    map_id[int(row['id'])] = row['text']
                    
rep = '/home/pedro/Documents/hci/rep/'

aux = []

with open(rep + 'Clustering_words_text.csv', 'wb') as f:
        writer = csv.writer(f)
        #reader = csv.DictReader(f)
        writer.writerow(["id","class", "text"])
        idx = 0
        #for row in aux:
            #aux.append([row['id'], row['label'], map_id[int(row['id'])].encode('utf-8')])
            #idx = idx + 1
        writer.writerows(aux)


count_all = Counter()
l = []

p = '/home/pedro/Documents/hci/rep/Clustering_words_text.csv'


with open(p) as f:
    reader = csv.DictReader(f)
    for row in reader:
        if int(row['class']) == 5:
            l.append(row['text'])

for e in l:
    terms_only = [term for term in preprocess(e.decode("utf-8"), True)
                      if term not in stop and
                      not term.startswith(('\\', 'https', 'http'))
                      and len(term) > 2 ]
    
    punctuation = list(string.punctuation)
    for p in punctuation:
                    terms_only = [x.replace(p, '') for x in terms_only]
    count_all.update(ngrams(terms_only, n))
    
count_all.most_common(30)
'''