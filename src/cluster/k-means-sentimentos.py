# -*- coding: utf-8 -*-
"""
Created on Thu Jan 25 20:56:20 2018

@author: pedro
"""

import sys
reload(sys)
sys.setdefaultencoding('utf-8')

import re

import scipy as sc

emoticons_str = r"""
    (?:
        [:=;] # Eyes
        [oO\-]? # Nose (optional)
        [D\)\]\(\]/\\OpP] # Mouth
    )"""
 
regex_str = [
    emoticons_str,
    r'<[^>]+>', # HTML tags
    r'(?:@[\w_]+)', # @-mentions
    r"(?:\#+[\w_]+[\w\'_\-]*[\w_]+)", # hash-tags
    r'http[s]?://(?:[a-z]|[0-9]|[$-_@.&amp;+]|[!*\(\),]|(?:%[0-9a-f][0-9a-f]))+', # URLs
 
    r'(?:(?:\d+,?)+(?:\.?\d+)?)', # numbers
    r"(?:[a-z][a-z'\-_]+[a-z])", # words with - and '
    r"(?:[0-9][0-9'\-_]+[0-9])",
    r'(?:[\w_]+)', # other words
    
# anything else
]
 
   
tokens_re = re.compile(r'('+'|'.join(regex_str)+')', re.VERBOSE | re.IGNORECASE)
emoticon_re = re.compile(r'^'+emoticons_str+'$', re.VERBOSE | re.IGNORECASE)

from nltk.corpus import stopwords
import string
 
punctuation = list(string.punctuation)
stop =  punctuation + ['rt', 'via', '\\', 'http'] + stopwords.words('portuguese') 
 
def tokenize(s):
    return tokens_re.findall(s)
 
def preprocess(s,lowercase=False, word=' '):
    tokens = s.split(' ')
    if lowercase:
        tokens = [token if emoticon_re.search(token) else token.lower() for token in tokens]
    return tokens
 
tweet = 'RT @marcobonzanini: just an example! :D http://example.com #NLP'
print(preprocess(tweet, 'just'))

from itertools import tee, islice

def ngrams(lst, n):
  tlst = lst
  while True:
    a, b = tee(tlst)
    l = tuple(islice(a, n))
    if len(l) == n:
      yield l
      next(b)
      tlst = b
    else:
      break
 
import csv

from os import listdir
from os.path import isfile, join
import operator
import json
from collections import Counter
import tqdm
from tqdm import *
import pandas
import json
import vincent
import os
import math
import scipy as sc
import numpy as np

mypath = '/home/pedro/Documents/hci/db/twitter_senadores/'

onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
onlyfiles.sort()

def tweet(e):
    s = []
    with open(mypath+e, 'r') as f:
        reader = csv.DictReader(f)
        for row in reader:
            terms_only = [term for term in preprocess(row['text'].decode("utf-8"), True)
                  if term not in stop and
                  not term.startswith(('\\', 'https', 'http', '#'))
                  and len(term) > 2 ]
            punctuation = list(string.punctuation)
            for p in punctuation:
                terms_only = [x.replace(p, '').lower() for x in terms_only]
            s.append(terms_only)
    return s


rep = '/home/pedro/Documents/hci/rep/'

s = []

idx = 0

set_list = {}

for e in onlyfiles:
    set_list[e] = np.zeros(13)

x = []
y = []

def get_twitter_cluster():
    global idx, x, y
    with open(rep+'list_cluster_result_4.csv', 'r') as f:
        reader = csv.DictReader(f)
        for idx, row in enumerate(reader):
            sen_pos = []
            sen_neg = []
            bigram = row['bigram']
            bigram = bigram.split(',')
            bigram = (bigram[0].decode('utf-8'),bigram[1].decode('utf-8'))
            for value, sent in enumerate(['senador_pos', 'senador_neg']):
                list_sen = row[sent]
                list_sen = list_sen.replace(', ', ',')[1:-1]
                list_sen = list_sen.split(',')
                list_sen = [term[1:-1] for term in list_sen]
                #set_list.append(list_sen)
                print '------------------------------------------------'
                print bigram
                #print list_sen
                
                for e in list_sen:
                    print '-------------------------------------------------'
                    print 'senador = ', e
                    if e in onlyfiles:
                        set_list[e][idx] = (-2) * value + 1

    for e in set_list:
        x.append(set_list[e])
        y.append(e)
        
    x = np.array(x)
               

            

result = []

get_twitter_cluster()

from sklearn.cluster import KMeans

list_la = []

for num in range(1, 40, 3):
    
    k = num
    
    # Number of clusters
    kmeans = KMeans(n_clusters=k, n_jobs=-1)
    # Fitting the input data
    kmeans = kmeans.fit(x)
    # Getting the cluster labels
    labels = kmeans.predict(x)
    
    list_la.append(kmeans.inertia_)    
    
    # Centroid values
    centroids = kmeans.cluster_centers_
    
    print 'k=', num, '- cost:', kmeans.inertia_

'''
with open(rep + 'list_cluster_result_4.csv', 'wb') as f:
    writer = csv.writer(f)
    writer.writerow(["bigram","quant_pos", "senador_pos", "quant_neg", "senador_neg"])
    for e in range(len(result)):
        writer.writerows(result)
'''

#%%

# clustering dataset
# determine k using elbow method
 
from sklearn.cluster import KMeans
from sklearn import metrics
from scipy.spatial.distance import cdist
import numpy as np
import matplotlib.pyplot as plt

import numpy as np

from sklearn.cluster import DBSCAN
from sklearn import metrics
from sklearn.datasets.samples_generator import make_blobs
from sklearn.preprocessing import StandardScaler
 



X = x
x2 = np.array(y)

pca = PCA(n_components=3)
X = pca.fit(X).transform(X)

# #############################################################################
# Compute DBSCAN
db = DBSCAN(eps=0.5, min_samples=2).fit(X)
core_samples_mask = np.zeros_like(db.labels_, dtype=bool)
core_samples_mask[db.core_sample_indices_] = True
labels = db.labels_

# Number of clusters in labels, ignoring noise if present.
n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)

print('Estimated number of clusters: %d' % n_clusters_)

print("Silhouette Coefficient: %0.3f"
      % metrics.silhouette_score(X, labels))

# #############################################################################
# Plot result
import matplotlib.pyplot as plt

# Black removed and is used for noise instead.
unique_labels = set(labels)
colors = [plt.cm.Spectral(each)
          for each in np.linspace(0, 1, len(unique_labels))]
for k, col in zip(unique_labels, colors):
    if k == -1:
        # Black used for noise.
        col = [0, 0, 0, 1]

    class_member_mask = (labels == k)

    xy = X[class_member_mask & core_samples_mask]
    plt.plot(xy[:, 0], xy[:, 1], 'o', markerfacecolor=tuple(col),
             markeredgecolor='k', markersize=14)

    xy = X[class_member_mask & ~core_samples_mask]
    plt.plot(xy[:, 0], xy[:, 1], 'o', markerfacecolor=tuple(col),
             markeredgecolor='k', markersize=6)

plt.title('Estimated number of clusters: %d' % n_clusters_)
plt.show()

#%%

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import pandas as pd
import numpy as np

v = np.random.rand(77,4)
v[:,3] = np.random.randint(0,2,size=77)
df = pd.DataFrame(v, columns=['Feature1', 'Feature2','Feature3',"Cluster"])
print (df)

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
x = [e[0] for e in X]
y = [e[1] for e in X]
z = [e[2] for e in X]

ax.scatter(x,y,z, marker="o",c = labels+1, s=40, cmap="RdBu")

plt.show()