# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

from os import listdir
from os.path import isfile, join

import operator 
import json
from collections import Counter
import tqdm
from tqdm import *

import csv

import data

mypath = '/home/pedro/Documents/hci/db/twitter_senadores/'

onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
onlyfiles.sort()
l = list()
for e in tqdm(onlyfiles):
        screen_name = e[:-11]
        with open(mypath+e) as csvfile:
            with open('/home/pedro/filtro_s/%s_filtrado.csv' % screen_name, 'wb') as f:
                writer = csv.writer(f)
                writer.writerow(["id","created_at","text"])
                reader = csv.DictReader(csvfile)
                try:
                    for row in reader:
                        if row['created_at'][:10] >= '2014-01-01':
                        #print row['created_at'][:10]
                            l.append([row['id'],row['created_at'],row['text']])
                except:
                    continue
                writer.writerows(l)
                l = list()
                        
