# -*- coding: utf-8 -*-
"""
Created on Tue Jan 09 10:29:13 2018

@author: pedro
"""

import sys
reload(sys)
sys.setdefaultencoding('utf-8')

import re

import scipy as sc

emoticons_str = r"""
    (?:
        [:=;] # Eyes
        [oO\-]? # Nose (optional)
        [D\)\]\(\]/\\OpP] # Mouth
    )"""
 
regex_str = [
    emoticons_str,
    r'<[^>]+>', # HTML tags
    r'(?:@[\w_]+)', # @-mentions
    r"(?:\#+[\w_]+[\w\'_\-]*[\w_]+)", # hash-tags
    r'http[s]?://(?:[a-z]|[0-9]|[$-_@.&amp;+]|[!*\(\),]|(?:%[0-9a-f][0-9a-f]))+', # URLs
 
    r'(?:(?:\d+,?)+(?:\.?\d+)?)', # numbers
    r"(?:[a-z][a-z'\-_]+[a-z])", # words with - and '
    r"(?:[0-9][0-9'\-_]+[0-9])",
    r'(?:[\w_]+)', # other words
    
# anything else
]
 
   
tokens_re = re.compile(r'('+'|'.join(regex_str)+')', re.VERBOSE | re.IGNORECASE)
emoticon_re = re.compile(r'^'+emoticons_str+'$', re.VERBOSE | re.IGNORECASE)

from nltk.corpus import stopwords
import string
 
punctuation = list(string.punctuation)
stop =  punctuation + ['rt', 'via', '\\', 'http'] + stopwords.words('portuguese') 
 
def tokenize(s):
    return tokens_re.findall(s)
 
def preprocess(s,lowercase=False, word=' '):
    tokens = s.split(' ')
    if lowercase:
        tokens = [token if emoticon_re.search(token) else token.lower() for token in tokens]
    return tokens
 
tweet = 'RT @marcobonzanini: just an example! :D http://example.com #NLP'
print(preprocess(tweet, 'just'))

from itertools import tee, islice

def ngrams(lst, n):
  tlst = lst
  while True:
    a, b = tee(tlst)
    l = tuple(islice(a, n))
    if len(l) == n:
      yield l
      next(b)
      tlst = b
    else:
      break
 
import csv

from os import listdir
from os.path import isfile, join
import operator
import json
from collections import Counter
import tqdm
from tqdm import *
import pandas
import json
import vincent
import os
import math
import scipy as sc
import numpy as np

mypath = '/home/pedro/Documents/hci/db/twitter_senadores/'

onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
onlyfiles.sort()

def tweet(e):
    s = []
    with open(mypath+e, 'r') as f:
        reader = csv.DictReader(f)
        for row in reader:
            terms_only = [term for term in preprocess(row['text'].decode("utf-8"), True)
                  if term not in stop and
                  not term.startswith(('\\', 'https', 'http', '#'))
                  and len(term) > 2 ]
            punctuation = list(string.punctuation)
            for p in punctuation:
                terms_only = [x.replace(p, '').lower() for x in terms_only]
            s.append(terms_only)
    return s

def count(n, e):
       
    count_all = Counter()
    #for e in tqdm(onlyfiles):
    #for e in onlyfiles:
    with open(mypath+e, 'r') as f:
            reader = csv.DictReader(f)
            for row in reader:
                terms_only = [term for term in preprocess(row['text'].decode("utf-8"), True)
                      if term not in stop and
                      not term.startswith(('\\', 'https', 'http'))
                      and len(term) > 2 ]
                punctuation = list(string.punctuation)
                for p in punctuation:
                    terms_only = [x.replace(p, '') for x in terms_only]
               
                count_all.update(ngrams(terms_only, n))
    return count_all
    
#w_good = [ 'positivo', 'afortunado', 'correto', 'superior', 'rapido', u'legítmo', u'constitucional', u'avanço', u'atacar', u'favor', u'aceitação', u'abre']
#w_bad = ['negativo', 'desafortunado', 'incorreto', 'inferior', 'lento', u'inlegítmo' , u'inconstitucional ', u'retrocesso', u'defender', u'contra', u'rejeição', u'fecha']



rep = '/home/pedro/Documents/hci/rep/'

s = []
with open(rep+'fdist_sen.csv', 'r') as f:
    reader = csv.DictReader(f)
    for row in reader:
        i = int(row['freq'])
        bigram = row['bigram'].replace(', ', ',')[1:-1]
        bigram = bigram.split(',')
        bigram = (bigram[0][2:-1],bigram[1][2:-1])
        if i > 20:
            print bigram, ' - ', i
        else:
            break

bigram = (u'cpi', u'animais')

def get_list(bigram):
    l = []
    for e in tqdm(onlyfiles):
        c = count(2, e)
        if bigram in c:
           if c[bigram] > 5:
               l.append(e)
    return l

'''

l = get_list(bigram) 
set_clust[bigram] = l

with open(rep + 'list_cluster.csv', 'wb') as f:
    writer = csv.writer(f)
    writer.writerow(["bigram","quant", "senador"])
    for e in set_clust:
        aux = [[str(e).encode("utf-8"),len(set_clust[e]), set_clust[e]]]
        writer.writerows(aux)

'''

def count_words(t):
    total_pos = 0 
    total_neg = 0
    total = 0
    
    for idx in range(len(w_good)):
        ref_pos = w_good[idx]
        ref_neg = w_bad[idx]
    
        for tweet in t:
            if ref_pos in tweet:
                total_pos = 1 + total_pos
                
            if ref_neg in tweet:
                total_neg = 1 + total_neg
                
            total = total + 1
        
    return total, total_pos, total_neg


def TMI_1(word, ref_pos, ref_neg, tweet, s, total, total_pos, total_neg):
    
    count_word_pos = 0
    count_word_neg = 0
    count_word = 0
        
    try:
        count_word_pos = s[(ref_pos, word)]
    except:
        count_word_pos = 0
        
    try:
        count_word_neg = s[(ref_neg, word)]
        
    except:
        count_word_neg = 0
        
    prob_pos = float(count_word_pos) / total_pos
    
    prob_neg = float(count_word_neg) / total_neg
            #prob = np.log2(prob) (float(count_word_pos)/total_pos) / 

    prob = 0
    
    if prob_pos == 0 and prob_neg == 0:
        prob = -1
    
    if prob_neg == 0:
        prob_neg = 0.01
        prob = float(prob_pos) / prob_neg
        prob = np.log2(prob)
    
    if prob_pos == 0:
        prob_pos = 0.01
        prob = float(prob_pos) / prob_neg
        prob = np.log2(prob)
    
    
    #print prob_pos
    #print prob_neg
    
    return count_word_pos, count_word_neg, prob



idx = 0

set_list = []

def get_twitter_cluster():
    global idx
    with open(rep+'list_cluster.csv', 'r') as f:
        reader = csv.DictReader(f)
        for row in reader:
            idx = idx + 1
            sen_pos = []
            sen_neg = []
            bigram = row['bigram'].replace(', ', ',')[1:-1]
            bigram = bigram.split(',')
            bigram = (bigram[0][2:-1].decode('utf-8'),bigram[1][2:-1].decode('utf-8'))
            list_sen = row['senador']
            list_sen = list_sen.replace(', ', ',')[1:-1]
            list_sen = list_sen.split(',')
            list_sen = [term[1:-1] for term in list_sen]
            #set_list.append(list_sen)
            print '------------------------------------------------'
            print bigram
            print list_sen
            
            for e in list_sen:
                t = tweet(e)
                c = count(2, e)
                total, total_pos, total_neg = count_words(t)
                print '-------------------------------------------------'
                print 'senador = ', e
                print 'total = ',total, 'pos = ', total_pos, 'neg=', total_neg
                tmi_p = 0                            
                tmi_n = 0  
                prob = 0                                          
                for twe in t:
                    if bigram[1] in twe:
                        if bigram[0] in twe:
                            #print twe
                            for twe_w in twe:
                                if twe_w in map_type:
                                    if u'Adj' in map_type[twe_w][:3] :
                                        s = O(twe_w, l)
                                        print 'word = ', twe_w, ' value = ', s
                        '''
                            for twe_w in twe:
                                for idx in range(len(w_good)):
                                    aux_p, aux_n, aux_prob = TMI_1(twe_w, w_good[idx], w_bad[idx], t, c, total, total_pos, total_neg)
                                    prob = prob + aux_prob
                                    
                                    if tmi_p != 0 :
                                        print 'Pos - word :', bigram, '-',w_good[idx], 'prob : ', tmi_p
                                    if tmi_n != 0 :
                                        print 'Neg - word :', bigram, '-',w_bad[idx], 'prob : ', tmi_n
                                        
                                #print 'TMI = ', prob
                if prob > 0 :
                    print 'sen = ', e , '(pos)'
                    sen_pos.append(e)
                if prob < 0:
                    sen_neg.append(e)
                    print 'sen= ', e , '(neg)'
                print '-------------------------------------------------'
                
                '''
            bigram_str = bigram[0].encode("utf-8") + ',' + bigram[1].encode("utf-8") 
            result.append([bigram_str.encode("utf-8"), len(sen_pos), sen_pos, len(sen_neg), sen_neg])
                #print 'pos = ', aux_p, 'neg=', aux_n, 'prob=', aux_prob
            #return

            

result = []

get_twitter_cluster()
            


with open(rep + 'list_cluster_result_4.csv', 'wb') as f:
    writer = csv.writer(f)
    writer.writerow(["bigram","quant_pos", "senador_pos", "quant_neg", "senador_neg"])
    for e in range(len(result)):
        writer.writerows(result)

