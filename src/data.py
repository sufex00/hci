# -*- coding: utf-8 -*-
"""
Created on Thu Dec 21 18:52:34 2017

@author: pedro
"""

import sys
reload(sys)
sys.setdefaultencoding('utf8')

str_t = '5. [Verbo] {enegrecer, escurecer} <4>'

map_t = {}

map_type = {}

trash = []

from nltk.corpus import stopwords
import tqdm
from tqdm import *

def has_word(word):
    return word in map_t

def get_list_t(word, l, trash = []):
    arr = []
    aux = map_t[word]
    for e_v in aux:
        e = l[e_v]
        for item in e._word_l:
            if item not in trash:
                arr.append(item)
    arr = filter(lambda a: a != word, arr)
    return arr

def find_distance(good, bad, l):
    level = 1
    trash = []
    list_t = get_list_t(good, l)
    #print list_t
    trash.append(good)
    aux = len(l)
    while bad not in list_t and level <= 20:
        aux_t = []
        for item in list_t:
            if item not in trash:
                s = get_list_t(item, l, trash)
                trash.append(item)
                aux_t = aux_t + s
        level = level + 1
        #print 'level :' ,level, 'Trash:' , len(trash), 'list:', len(list_t) , '\n' 
       # print '-----------------------------------------------------\n'
        list_t = list_t + aux_t
    if level <= 20:
        return level
    else :
        return 'fail'
    
def metrics_word(word, seed_pos, seed_neg, l, idx):
    try:
        return_t = find_distance(word, seed_neg, l) - find_distance(word, seed_pos, l)
        return_t = float(return_t) / look_uptable[idx]
    except:
        return_t = 'fail'
    return return_t

w_good = ['bom',  'positivo',  'certo']
w_bad = ['ruim', 'negativo', 'errado']

def O(word, l):
    s = 0
    for idx in range(1):
        try:
            m =  metrics_word(word, w_good[idx], w_bad[idx], l, idx)
            #print m
            s = s + m
        except:
            return 'fail'
    return s
    


class Data(object):
     def __init__(self, str_t):
             str_t = str_t.replace('{', '')
             str_t = str_t.replace('}', '')
             str_t = str_t.replace('\r\n', '')
             str_t = str_t.replace(',', '')
             self._id = int(str_t.split(' ')[0][:-1])
             self._type = str_t.split(' ')[1][1:-1]
             self._word_l = list()
             for e in range(2, len(str_t.split(' '))-1):
                 self._word_l.append(str_t.split(' ')[e].decode("iso-8859-1"))
                 try:
                     map_t [str_t.split(' ')[e].decode("iso-8859-1")].append(self._id)
                     map_type [str_t.split(' ')[e].decode("iso-8859-1")] = self._type
                 except:
                     map_t [str_t.split(' ')[e].decode("iso-8859-1")] = list()
                     map_t [str_t.split(' ')[e].decode("iso-8859-1")].append(self._id)
             aux = str_t.split(' ')[len(str_t.split(' '))-1]
             if aux[0] == '<':
                 self._ant = int(aux[1:-1])
             else:
                 self._ant = 'Void'
                 self._word_l.append(aux.decode("iso-8859-1"))
                 map_type [aux.decode("iso-8859-1")] = self._type
                 try:
                     map_t[aux.decode("iso-8859-1")].append(self._id)
                 except:
                     map_t [aux.decode("iso-8859-1")] = list()
                     map_t [aux.decode("iso-8859-1")].append(self._id)
                     

     def __str__(self):
         return 'Id : '+ str(self._id) + '\nType: '+'\nWord:'+str(self._word_l)+'\nTipo: '+ self._type + '\nAnt:'+ str(self._ant)
frase = 'Inaceitável Por isso apresentei Projeto de Decreto Legislativo suspender os efeitos da medida de Temer'

look_uptable = []
l = range(50000)
map_o={}
def main():
    # my code here
    stop = stopwords.words('portuguese')
    
    with open('../db/base_tep2/base_tep2.txt') as f:
        for line in f:
            aux = Data(line)
            l[aux._id] = aux
            #print Data(line)
            #time.sleep(0.1)
    print l[3341]._ant
    print get_list_t(u'alegre', l)
    print has_word(u'brilhante')
    print has_word(u'ruim')
    
    
    for idx in range(len(w_good)):
        print idx
        look_uptable.append(find_distance(w_good[idx], w_bad[idx], l))
    aux = []
    '''
    for idx, e in tqdm(enumerate(s)):
            if idx % 15 == 0:
                value = O(e, l)
                map_o[e] = value
                aux.append([e.encode("utf-8"), value])
        
    with open('/home/pedro_barros/Documents/hci/db/words_1.csv', 'wb') as f:    
         writer = csv.writer(f)
         writer.writerow(["value", "id"])
         writer.writerows(aux)
    '''

    #print find_distance(u'roubo', u'incorreto', l)
    #print metrics_word(u'atrasado', w_good[0], w_bad[0], l)
    #print O(u'crítica', l)
    #s = 0
    #for e in frase.split(' '):
     #   print e
      #  if has_word(e):
       #     s = metrics_word(e, w_good[0], w_bad[0], l) + s
        #    print 'w[',e,'()]=',s

if __name__ == "__main__":
    main()

