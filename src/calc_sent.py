
# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

from os import listdir
from os.path import isfile, join

import operator 
import json
from collections import Counter
import tqdm
from tqdm import *

import csv

import sys
reload(sys)
sys.setdefaultencoding('utf8')

import re

import scipy as sc

emoticons_str = r"""
    (?:
        [:=;] # Eyes
        [oO\-]? # Nose (optional)
        [D\)\]\(\]/\\OpP] # Mouth
    )"""
 
regex_str = [
    emoticons_str,
    r'<[^>]+>', # HTML tags
    r'(?:@[\w_]+)', # @-mentions
    r"(?:\#+[\w_]+[\w\'_\-]*[\w_]+)", # hash-tags
    r'http[s]?://(?:[a-z]|[0-9]|[$-_@.&amp;+]|[!*\(\),]|(?:%[0-9a-f][0-9a-f]))+', # URLs
 
    r'(?:(?:\d+,?)+(?:\.?\d+)?)', # numbers
    r"(?:[a-z][a-z'\-_]+[a-z])", # words with - and '
    r"(?:[0-9][0-9'\-_]+[0-9])",
    r'(?:[\w_]+)', # other words
    
# anything else
]
 
   
tokens_re = re.compile(r'('+'|'.join(regex_str)+')', re.VERBOSE | re.IGNORECASE)
emoticon_re = re.compile(r'^'+emoticons_str+'$', re.VERBOSE | re.IGNORECASE)

from nltk.corpus import stopwords
import string
 
punctuation = list(string.punctuation)
stop =  punctuation + ['rt', 'via', '\\', 'http'] + stopwords.words('portuguese') 
 
def tokenize(s):
    return tokens_re.findall(s)
 
def preprocess(s,lowercase=False, word=' '):
    tokens = s.split(' ')
    if lowercase:
        tokens = [token if emoticon_re.search(token) else token.lower() for token in tokens]
    return tokens
 
tweet = 'RT @marcobonzanini: just an example! :D http://example.com #NLP'
print(preprocess(tweet, 'just'))

from itertools import tee, islice

def ngrams(lst, n):
  tlst = lst
  while True:
    a, b = tee(tlst)
    l = tuple(islice(a, n))
    if len(l) == n:
      yield l
      next(b)
      tlst = b
    else:
      break
 
import csv

from os import listdir
from os.path import isfile, join
import operator
import json
from collections import Counter
import tqdm
from tqdm import *
import pandas
import json
import vincent
import os
import math
import scipy as sc
import numpy as np

mypath = '/home/pedro_barros/Documents/hci/db/twitter_senadores/'

onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
onlyfiles.sort()

def tweet(e):
    s = []
    with open(mypath+e, 'r') as f:
        reader = csv.DictReader(f)
        for row in reader:
            terms_only = [term for term in preprocess(row['text'].decode("utf-8"), True)
                  if term not in stop and
                  not term.startswith(('\\', 'https', 'http'))
                  and len(term) > 2 ]
            punctuation = list(string.punctuation)
            for p in punctuation:
                terms_only = [x.replace(p, '') for x in terms_only]
            s.append(terms_only)
    return s


hasWord = set()

for e in tqdm(onlyfiles):
    t = tweet(e)
    for p in t:
        for w in p:
            if w not in hasWord:
                hasWord.add(w)

s = set()

for e in hasWord:
    if e in map_t:
        s.add(e)
        
look_uptable = []

for idx in range(len(w_good)):
    find_distance(w_good[idx], w_bas[idx], l)

for e in tqdm(onlyfiles):
        screen_name = e[:-11]
        with open(mypath+e) as csvfile:
            with open('/home/pedro/filtro_s/%s_filtrado.csv' % screen_name, 'wb') as f:
                writer = csv.writer(f)
                writer.writerow(["id","created_at","text"])
                reader = csv.DictReader(csvfile)
                try:
                    for row in reader:
                        if row['created_at'][:10] >= '2014-01-01':
                        #print row['created_at'][:10]
                            l.append([row['id'],row['created_at'],row['text']])
                except:
                    continue
                writer.writerows(l)
                l = list()
                        